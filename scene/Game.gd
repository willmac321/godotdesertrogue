extends Node2D

const TILE_SIZE = 32

const LEVEL_SIZES =	[
		Vector2(30,30),
		Vector2(35,35),
		Vector2(40,40),
		Vector2(45,45),
		Vector2(50,50),
	]

const LEVEL_ROOM_COUNTS = [5,7,9,12,15]
const MIN_ROOM_DIM = 8
const MAX_ROOM_DIM = 15
const CORRIDOR_WIDTH = 2

enum Tile {Desert, Empty}

# Current level -----------------------------

var level_num = 0
var map = []
var rooms = []
var level_size

# Node Refs----------------------------------

onready var tile_map = $TileMap
onready var player = $Player

# Game State --------------------------------

var player_tile
var score = 0

func _ready():
	OS.set_window_size(Vector2(1280, 720));
	OS.set_window_position(OS.get_screen_size()*0.5 - OS.get_window_size()*0.5);
	randomize()
	build_level()

func build_level():
	rooms.clear()
	map.clear()
	tile_map.clear()

	level_size = LEVEL_SIZES[level_num]
	for x in range(level_size.x):
		map.append([])
		for y in range(level_size.y):
			map[x].append(Tile.Empty)
			tile_map.set_cell(x, y, Tile.Empty)

	var free_regions = [Rect2(Vector2(CORRIDOR_WIDTH + 1, CORRIDOR_WIDTH + 1), level_size - Vector2(CORRIDOR_WIDTH + 1, CORRIDOR_WIDTH + 1))]

	var num_rooms = LEVEL_ROOM_COUNTS[level_num]

	for i in range(num_rooms):
		add_room(free_regions)
		if free_regions.empty():
			break

	tile_map.update_bitmask_region()

func add_room(free_regions):
	var region = free_regions[randi() % free_regions.size()]
	var size_x = MIN_ROOM_DIM

	if region.size.x > MIN_ROOM_DIM:
		size_x += randi() % int(region.size.x - MIN_ROOM_DIM)

	var size_y = MIN_ROOM_DIM

	if region.size.y > MIN_ROOM_DIM:
		size_y += randi() % int(region.size.y - MIN_ROOM_DIM)

	size_x = min(size_x, MAX_ROOM_DIM)
	size_y = min(size_y, MAX_ROOM_DIM)

	var start_x = region.position.x
	var start_y = region.position.y

	if region.size.x > size_x:
		start_x += randi() % int(region.size.x - size_x)
	if region.size.y > size_y:
		start_y += randi() % int(region.size.y - size_y)

	var room = Rect2(start_x, start_y, size_x, size_y)
	rooms.append(room)

	for x in range(start_x, start_x + size_x):
		for y in range(start_y + 1, start_y + size_y - 1):
			set_tile(x, y, Tile.Desert)
	for x in range(start_x, start_x + size_x):
		for y in range(start_y + 1, start_y + size_y - 1):
			var blank = randi() % int(100)
			if (blank >= 80 && isOkToMakeBlank(x, y)):
				set_tile(x, y, Tile.Empty)

	cut_regions(free_regions, room)

func isOkToMakeBlank(x, y):
	for i in range(x - CORRIDOR_WIDTH, x + CORRIDOR_WIDTH + 1):
		for j in range(y - CORRIDOR_WIDTH, y + CORRIDOR_WIDTH + 1):
			if i >= 0 && j >= 0 && i <= level_size.x && j<= level_size.y && map[i][j] == Tile.Empty:
				print(i, ' ', j)
				return false
	return true


func cut_regions(free_regions, region_to_remove):
	var removal_queue = []
	var add_queue = []

	for region in free_regions:
		if region.intersects(region_to_remove):
			removal_queue.append(region)

			var leftover_left = region_to_remove.position.x - region.position.x - 1
			var leftover_right = region.end.x - region_to_remove.end.x - 1
			var leftover_above = region_to_remove.position.y - region.position.y - 1
			var leftover_below = region.end.y - region_to_remove.end.y - 1

			if leftover_left >= MIN_ROOM_DIM:
				add_queue.append(Rect2(region.position, Vector2(leftover_left, region.size.y)))
			if leftover_right >= MIN_ROOM_DIM:
				add_queue.append(Rect2(Vector2(region_to_remove.end.x + 1, region.position.y), Vector2(leftover_right, region.size.y)))
			if leftover_above >= MIN_ROOM_DIM:
				add_queue.append(Rect2(region.position, Vector2(region.size.x, leftover_above)))
			if leftover_below >= MIN_ROOM_DIM:
				add_queue.append(Rect2(Vector2(region.position.x, region_to_remove.end.y + 1), Vector2(region.size.x, leftover_below)))

	for region in removal_queue:
		free_regions.erase(region)
	for region in add_queue:
		free_regions.append(region)

func set_tile(x, y, type):
	map[x][y] = type
	tile_map.set_cell(x, y, type)
